const prizeNum = consts.prizeList.length
const perAngle = 360 / prizeNum
const offsetAngle = perAngle / 2
const circleCount = 4 //旋转圈数
const rotateDuration = 3 // 持续时间
const panel = document.querySelector('.luckpanel')

let isRotating = false


function drawPanel() { //绘制圆盘骨架
  const canvas = document.querySelector('#canvas')
  const ctx = canvas.getContext('2d')
  const w = canvas.clientWidth
  const h = canvas.clientHeight
  const dpr = window.devicePixelRatio
  // 处理设备分辨率
  canvas.width = w * dpr
  canvas.height = h * dpr
  ctx.scale(dpr, dpr)

  // 将画布逆时针旋转90°
  ctx.translate(0, h) //平移 往水平方向和垂直方向移动0,h位置
  ctx.rotate(-90 * Math.PI / 180) //
  ctx.strokeStyle = consts.borderColor;

  const perRadian = (Math.PI * 2) / prizeNum; //每一份的角度差
  for (let i = 0; i < prizeNum; i++) {
    const radian = perRadian * i; //开始绘制的角度

    ctx.beginPath() //创建路径
    ctx.fillStyle = consts.prizeBgColors[i]; //填充色
    ctx.moveTo(w / 2, h / 2) //从canvas中心作为起始点
    //创建一个圆 (x坐标,y坐标,半径,(起始角以弧度计),(结束角以弧度计),(可选顺时针/逆时针))
    ctx.arc(w / 2, h / 2, w / 2, radian, radian + perRadian, false) // 顺时针创建
    ctx.closePath() //绘制线条以返回开始点
    ctx.stroke() //绘制线条
    ctx.fill() //填充
  }
}

function getPrizeItem({
  name,
  src
}) {
  const el = document.createElement('div')
  const tpl = `
    <div class="prize-item">
      <div class="prize-item__name">${name}</div>
      <div class="prize-item__img">
        <img src="${src}" alt="">
      </div>
    </div>
  `
  el.innerHTML = tpl

  return el.firstElementChild
}
//填充奖品内容
function fillPrize() {
  const container = document.querySelector('.prize');
  consts.prizeList.forEach((item, i) => {
    const el = getPrizeItem({
      name: item.prizeName,
      src: item.prizeImg
    })

    // 旋转
    const currentAngle = perAngle * i + offsetAngle;
    el.style.transform = `rotate(${currentAngle}deg)`; //旋转居中
    container.appendChild(el)
  })
}

let startRotateAngle = 0; //当前旋转的度数

function rotate(index) {
  const rotateAngle = (
    startRotateAngle +
    circleCount * 360 + //总圈数*360 = 需要旋转的总度数
    360 - (perAngle * index + offsetAngle) - //总度数 - 被选中item的度数 - 当前轮盘的度数
    startRotateAngle % 360
  );

  startRotateAngle = rotateAngle;
  panel.style.transform = `rotate(${rotateAngle}deg)`;

  panel.style.transitionDuration = `${rotateDuration}s`;

  // disX = 1;
  // // 缓冲效果  
  // timer = setInterval(function () {
  //   disX *= 0.95;
  //   rotateAngle += disX * 0.5;
  //   // rotateAngle
  //   panel.style.transform = `rotate(${rotateAngle}deg)`;
  //   if (Math.abs(disX) < 0.1) {
  //     clearInterval(timer);
  //   }
  // }, 17)


  setTimeout(() => {
    rotateEnd(index)
  }, rotateDuration * 1000);
}

//轮盘结束后的回调
function rotateEnd(index) {
  isRotating = false
  alert(consts.prizeList[index].prizeName)
}

function bindEvent() {
  document.querySelector('.pointer').addEventListener('click', function () {
    if (isRotating) { //避免重复点击
      return
    } else {
      isRotating = true
    }

    const index = Math.floor(Math.random() * prizeNum) //最终选中指定下标
    rotate(5)
  })
}

function init() {
  drawPanel()
  fillPrize()
  bindEvent()
}

document.addEventListener('DOMContentLoaded', init)